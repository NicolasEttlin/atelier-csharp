﻿//==========================================================
// WFTortoise
// Small Windows Forms program featuring a tortoise that the
// user can move.
// Author  : Nicolas Ettlin <nicolas.ettln@eduge.ch>
// Version : 1.0 (final) - 2018-04-25
//==========================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFTortoise
{
    /// <summary>
    /// Main form (shows the tortoise)
    /// </summary>
    public partial class frmMain : Form
    {
        private Tortoise tortoise = new Tortoise();

        public frmMain()
        {
            InitializeComponent();

            Paint += tortoise.Paint;
        }

        /// <summary>
        /// Update the form content
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Tick(object sender, EventArgs e)
        {
            Invalidate();
        }

        /// <summary>
        /// Enable anti-aliasing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
        }

        /// <summary>
        /// Remember the current position
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemember_Click(object sender, EventArgs e)
        {
            tortoise.RememberCurrentPosition();
        }

        /// <summary>
        /// Move the turtle using the numeric keypad
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.NumPad8:
                    tortoise.Move(0, -1);
                    break;
                case Keys.NumPad2:
                    tortoise.Move(0, 1);
                    break;
                case Keys.NumPad6:
                    tortoise.Move(1, 0);
                    break;
                case Keys.NumPad4:
                    tortoise.Move(-1, 0);
                    break;

                case Keys.NumPad7:
                    tortoise.Move(-1, -1);
                    break;
                case Keys.NumPad9:
                    tortoise.Move(1, -1);
                    break;
                case Keys.NumPad1:
                    tortoise.Move(-1, 1);
                    break;
                case Keys.NumPad3:
                    tortoise.Move(1, 1);
                    break;
            }
        }

        /// <summary>
        /// Change the color of the line between the tortoise and the remembered point
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnColor_Click(object sender, EventArgs e)
        {
            tortoise.SelectedLineColor = (sender as Button).ForeColor;
        }

        private void btnForget_Click(object sender, EventArgs e)
        {
            tortoise.RememberedPositions.Clear();
        }
    }
}
