﻿//==========================================================
// WFTortoise
// Small Windows Forms program featuring a tortoise that the
// user can move.
// Author  : Nicolas Ettlin <nicolas.ettln@eduge.ch>
// Version : 1.0 (final) - 2018-04-25
//==========================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace WFTortoise
{
    /// <summary>
    /// Tortoise with a current position and a remembered one
    /// </summary>
    class Tortoise
    {
        private Color selectedLineColor = Color.Red;

        /// <summary>
        /// Turtle size
        /// </summary>
        public Size Size { get; } = new Size(20, 20);

        /// <summary>
        /// Current turtle position
        /// </summary>
        public Point Position { get; private set; }

        /// <summary>
        /// Remembered position
        /// </summary>
        public List<RememberedPosition> RememberedPositions { get; private set; } = new List<RememberedPosition>();

        /// <summary>
        /// Selected color of the line between the tortoise and the remembered position
        /// </summary>
        public Color SelectedLineColor
        {
            get => selectedLineColor; set
            {
                selectedLineColor = value;

                // Update the last remembered point
                if (RememberedPositions.Count > 0)
                    RememberedPositions[RememberedPositions.Count - 1].Color = value;
            }
        }

        /// <summary>
        /// Creates a new tortoise with a default position
        /// </summary>
        public Tortoise() : this(new Point(100, 100))
        { }

        /// <summary>
        /// Creates a tortoise with a specific position
        /// </summary>
        /// <param name="position">Initial position</param>
        public Tortoise(Point position)
        {
            Position = position;
        }

        /// <summary>
        /// Move the tortoise
        /// </summary>
        /// <param name="x">Horizontal move</param>
        /// <param name="y">Vertical move</param>
        public void Move(int x, int y)
        {
            Position = new Point()
            {
                X = Position.X + (x * Size.Width),
                Y = Position.Y + (y * Size.Height),
            };
        }

        /// <summary>
        /// Set the current position as the remembered one
        /// </summary>
        public void RememberCurrentPosition()
        {
            RememberedPositions.Add(new RememberedPosition(Position, SelectedLineColor));
        }

        /// <summary>
        /// Paint the tortoise and the line between the tortoise and the remembered position
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Paint(object sender, PaintEventArgs e)
        {
            for (int i = 0; i < RememberedPositions.Count; i++)
            {
                RememberedPosition current = RememberedPositions[i];
                Point nextPoint = (i + 1 == RememberedPositions.Count) ? Position : RememberedPositions[i + 1].Point;
                e.Graphics.DrawLine(new Pen(current.Color), current.Point, nextPoint);
            }

            // Draw the tortoise
            e.Graphics.FillEllipse(Brushes.Green, new Rectangle(Position.X - Size.Width / 2, Position.Y - Size.Height / 2, Size.Width, Size.Height));
        }

    }
}
