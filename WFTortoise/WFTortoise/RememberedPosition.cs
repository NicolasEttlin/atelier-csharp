﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace WFTortoise
{
    class RememberedPosition
    {
        public Point Point { get; private set; }

        public Color Color { get; set; }

        public RememberedPosition(Point point, Color color)
        {
            Point = point;
            Color = color;
        }
    }
}
