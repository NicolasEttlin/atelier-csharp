﻿namespace WFTortoise
{
    partial class frmMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.btnRemember = new System.Windows.Forms.Button();
            this.btnColorRed = new System.Windows.Forms.Button();
            this.btnColorBlue = new System.Windows.Forms.Button();
            this.btnForget = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // btnRemember
            // 
            this.btnRemember.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemember.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemember.Location = new System.Drawing.Point(548, 13);
            this.btnRemember.Name = "btnRemember";
            this.btnRemember.Size = new System.Drawing.Size(114, 23);
            this.btnRemember.TabIndex = 0;
            this.btnRemember.Text = "Se rappeler du point";
            this.btnRemember.UseVisualStyleBackColor = true;
            this.btnRemember.Click += new System.EventHandler(this.btnRemember_Click);
            // 
            // btnColorRed
            // 
            this.btnColorRed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnColorRed.ForeColor = System.Drawing.Color.Red;
            this.btnColorRed.Location = new System.Drawing.Point(548, 71);
            this.btnColorRed.Name = "btnColorRed";
            this.btnColorRed.Size = new System.Drawing.Size(53, 23);
            this.btnColorRed.TabIndex = 1;
            this.btnColorRed.Text = "Rouge";
            this.btnColorRed.UseVisualStyleBackColor = true;
            this.btnColorRed.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnColorBlue
            // 
            this.btnColorBlue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnColorBlue.ForeColor = System.Drawing.Color.Blue;
            this.btnColorBlue.Location = new System.Drawing.Point(609, 71);
            this.btnColorBlue.Name = "btnColorBlue";
            this.btnColorBlue.Size = new System.Drawing.Size(53, 23);
            this.btnColorBlue.TabIndex = 2;
            this.btnColorBlue.Text = "Bleu";
            this.btnColorBlue.UseVisualStyleBackColor = true;
            this.btnColorBlue.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnForget
            // 
            this.btnForget.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnForget.Location = new System.Drawing.Point(548, 42);
            this.btnForget.Name = "btnForget";
            this.btnForget.Size = new System.Drawing.Size(114, 23);
            this.btnForget.TabIndex = 3;
            this.btnForget.Text = "Tout oublier";
            this.btnForget.UseVisualStyleBackColor = true;
            this.btnForget.Click += new System.EventHandler(this.btnForget_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 469);
            this.Controls.Add(this.btnForget);
            this.Controls.Add(this.btnColorBlue);
            this.Controls.Add(this.btnColorRed);
            this.Controls.Add(this.btnRemember);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmMain";
            this.Text = "Tortue";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmMain_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMain_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button btnRemember;
        private System.Windows.Forms.Button btnColorRed;
        private System.Windows.Forms.Button btnColorBlue;
        private System.Windows.Forms.Button btnForget;
    }
}

