﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;

namespace WFSnake
{
    class TitleScreenLabel : Label
    {
        Stopwatch timeSinceCreation = Stopwatch.StartNew();
        private int transitionTimeMs;
        private int stillTimeMs;

        string[] texts = { "hiss", "i am darkness", "snek intensifies", "nom nom", "sans ogm", $"v{Application.ProductVersion} !" };

        /// <summary>
        /// Know if the label is obsolete (i.e. the animation has ended)
        /// </summary>
        public bool Obsolete
            => true || timeSinceCreation.ElapsedMilliseconds > transitionTimeMs * 2 + stillTimeMs;

        public TitleScreenLabel(Random rnd) : base()
        {
            Text = texts[rnd.Next(texts.Length)];
            Font = new Font("Arial", 12);
            AutoSize = true;

            // Random appear time
            transitionTimeMs = rnd.Next(500, 1500);
            stillTimeMs = rnd.Next(1000, 2000);

            // Random position
            Top = rnd.Next(200);
            Left = rnd.Next(700);
        }

        public void UpdateStatus() {
            long currentMs = timeSinceCreation.ElapsedMilliseconds;

            int color = 0;

            if (currentMs < transitionTimeMs) // Fading in
                color = 255 - (int)Math.Round(((double)currentMs / transitionTimeMs) * 255);

            if (currentMs > (transitionTimeMs + stillTimeMs)) // Fading out
                color = Math.Min(255, (int)Math.Round(((double)(currentMs - transitionTimeMs - stillTimeMs) / transitionTimeMs) * 255));

            ForeColor = Color.FromArgb(color, color, color);
        }
    }
}
