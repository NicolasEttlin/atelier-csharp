﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFSnake
{
    public partial class frmGame : Form
    {
        private Game game;
        private ComputerPlayer computerPlayer;

        public frmGame(int playersCount)
        {
            game = new Game(playersCount);
            computerPlayer = new ComputerPlayer(game);

            InitializeComponent();

            Paint += game.Paint;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            lblStatus.Text = game.Status;
            Invalidate();
        }

        private void frm_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(Color.White);
        }

        private void tick_Tick(object sender, EventArgs e)
        {
            if (game.IsOver)
                return;

            // computerPlayer.Play();
            game.Tick();
        }

        private void frmGame_KeyDown(object sender, KeyEventArgs e)
        {
            foreach (Snake s in game.Snakes)
            {
                bool handled = s.HandleKeyPress(e.KeyCode);

                if (handled)
                    return;
            }
        }

        /// <summary>
        /// React to a key press using a snake's specific keymap
        /// </summary>
        /// <param name="keyCode">The pressed key</param>
        /// <param name="snake">The snake affected by the keymap</param>
        /// <param name="keys">The keymap (in order : up, down, left, right)</param>
        private void ReactToKeyPress(Keys keyCode, Snake snake, Keys[] keys) {
            Direction[] directions = { Direction.Up, Direction.Down, Direction.Left, Direction.Right };

            for (int i = 0; i < directions.Length; i++)
                if (keys[i] == keyCode)
                    snake.RequestDirectionChange(directions[i]);
        }
    }
}
