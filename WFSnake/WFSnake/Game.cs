﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace WFSnake
{
    class Game
    {
        public const int GRID_WIDTH = 20;
        public const int GRID_HEIGHT = 16;
        public const bool DRAW_GRID = false;
        public const int SECONDS_BEFORE_START = 3;

        Stopwatch gameStart = Stopwatch.StartNew();
        Random rnd = new Random();
        List<Apple> apples = new List<Apple>();

        /// <summary>
        /// Create a new game instance
        /// </summary>
        /// <param name="playersCount">Players count</param>
        public Game(int playersCount)
        {
            Color[] colors = {
                Color.FromArgb(255, 246, 83, 20),
                Color.FromArgb(255, 124, 187, 0),
                Color.FromArgb(255, 0, 161, 241),
                Color.FromArgb(255, 255, 187, 0),
            };

            List<Keys[]> allKeymaps = new List<Keys[]> {
                new Keys[] { Keys.W, Keys.A, Keys.S, Keys.D },
                new Keys[] { Keys.I, Keys.J, Keys.K, Keys.L },
                new Keys[] { Keys.Up, Keys.Left, Keys.Down, Keys.Right },
                new Keys[] { Keys.NumPad8, Keys.NumPad4, Keys.NumPad5, Keys.NumPad6 },
            };

            List<int> keymapsAttributionOrder = new List<int>{ 0, 2, 3, 1 };
            List<Keys[]> attributedKeymaps = keymapsAttributionOrder
                .Take(playersCount)
                .Select(k => allKeymaps[k])
                .ToList();

            List<Keys[]> keymaps = allKeymaps
                .Where(k => attributedKeymaps.Contains(k))
                .ToList();

            for (int i = 0; i < playersCount; i++)
                Snakes.Add(new Snake((int)((GRID_HEIGHT - 1) * (i + 0.5) / playersCount))
                {
                    Color = colors[i % colors.Length],
                    Keymap = keymaps[i % keymaps.Count]
                });
        }

        public List<Snake> Snakes { get; set; } = new List<Snake>();

        public List<Snake> ActiveSnakes
            => Snakes.Where(s => !s.IsInvincible).ToList();

        public List<Snake> HurtSnakes
        {
            get
            {
                List<Snake> result = new List<Snake>();

                foreach (Snake snake in ActiveSnakes)
                {
                    List<Snake> otherSnakes = ActiveSnakes.Where(s => s != snake).ToList();
                    List<Point> collidablePoints = snake.Elements // Self collision
                        .GetRange(0, snake.Elements.Count - 1) // Elements without the head
                        .Concat(otherSnakes.Select(s => s.Elements).SelectMany(p => p)) // Elements of the other snakes
                        .ToList();

                    List<Point> collisions = collidablePoints
                        .Where(e => e.X == snake.HeadPosition.X && e.Y == snake.HeadPosition.Y)
                        .ToList();

                    if (collisions.Count > 0)
                        result.Add(snake);
                }

                return result;
            }
        }

        public bool HasStarted
            => gameStart.Elapsed.TotalSeconds > SECONDS_BEFORE_START;

        /// <summary>
        /// Is the game over
        /// </summary>
        public bool IsOver
            => /* IsLost || */ IsWon;

        /// <summary>
        /// Is the game lost (collision)
        /// </summary>
        public bool IsLost
            => HurtSnakes.Count > 0;

        public bool IsWon
            => PossibleNewApplePositions.Count == 0;

        public string Status
            => $"Score : {String.Join(" / ", Snakes.Select(s => s.Score).ToArray())}";

        private List<Point> PossibleNewApplePositions
        {
            get
            {
                List<Point> cells = new List<Point>();
                for (int x = 0; x < GRID_WIDTH; x++)
                    for (int y = 0; y < GRID_HEIGHT; y++)
                        cells.Add(new Point(x, y));

                return cells
                    .Except(apples.Select(a => a.Position))
                    .Except(Snakes.Select(s => s.Elements).SelectMany(p => p).ToList())
                    .ToList();
            }
        }

        /// <summary>
        /// Draw the game
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            SizeF s = e.Graphics.VisibleClipBounds.Size;

            Pen pen = Pens.DarkGray;
            Random rnd = new Random();

            // Draw the grid
            if (DRAW_GRID)
                for (int x = 0; x < GRID_WIDTH; x++)
                    for (int y = 0; y < GRID_HEIGHT; y++)
                        e.Graphics.DrawRectangles(pen, new[] { GetGridCellCoordinates(s, x, y) }); // https://stackoverflow.com/a/49504863

            // Draw the apples
            foreach (Apple a in apples)
                e.Graphics.FillEllipse(Brushes.DarkRed, GetGridCellCoordinates(s, a.Position));

            // Draw the snakes elements
            foreach (Snake snake in Snakes)
                for (int i = 0; i < snake.Elements.Count; i++)
                    snake.DrawElement(e.Graphics, GetGridCellCoordinates(s, snake.Elements[i]), i);

            // Before the game start
            if (!HasStarted)
            {
                // Draw the keymaps
                foreach (Snake snake in Snakes)
                {
                    RectangleF cell = GetGridCellCoordinates(s, new Point(snake.Elements[snake.Elements.Count - 1].X + 1, snake.Elements[snake.Elements.Count - 1].Y));
                    snake.DrawKeymap(e.Graphics, new Point((int)cell.X, (int)cell.Y));
                }

                // Draw the game start countdown
                DrawCountdown(e.Graphics);
            }
        }

        /// <summary>
        /// Move the snake elements
        /// </summary>
        public void Tick()
        {
            if (!HasStarted)
                return;

            foreach (Snake snake in Snakes)
                snake.PerformDirectionChange();

            if (apples.Count == 0) // Add a new fruit
            {
                List<Point> positions = PossibleNewApplePositions;
                Point position = positions[rnd.Next(positions.Count)];
                apples.Add(new Apple(position));
            }

            foreach (Snake snake in Snakes)
            {
                // Hit an apple
                Point head = snake.NextHeadPosition;
                List<Apple> hitApples = apples.Where(a
                    => a.Box.IntersectsWith(new Rectangle(head, new Size(1, 1)))
                ).ToList();

                foreach (Apple a in hitApples)
                {
                    apples.Remove(a);
                    snake.Score += 1;
                }

                // Move the snake
                snake.Move(hitApples.Count == 0);
            }

            foreach (Snake s in HurtSnakes)
                s.Hurt();

            if (IsOver)
            {
                MessageBox.Show(IsLost ? "Game over" : "Bravo !");
                Application.Exit();
            }
        }

        /// <summary>
        /// Get a cell's coordinates
        /// </summary>
        /// <param name="gridSize">The grid size</param>
        /// <param name="x">The cell X position</param>
        /// <param name="y">The cell Y position</param>
        /// <returns>The position and size of the cell</returns>
        private RectangleF GetGridCellCoordinates(SizeF gridSize, int x, int y)
        {
            float cellWidth = gridSize.Width / GRID_WIDTH;
            float cellHeight = gridSize.Height / GRID_HEIGHT;

            float cellX = x * cellWidth;
            float cellY = y * cellHeight;

            return new RectangleF(new PointF(cellX, cellY), new SizeF(cellWidth, cellHeight));
        }

        /// <summary>
        /// Get a cell's coordinates
        /// </summary>
        /// <param name="gridSize">The grid size</param>
        /// <param name="position">The cell position</param>
        /// <returns>The position and size of the cell</returns>
        private RectangleF GetGridCellCoordinates(SizeF gridSize, Point position)
        {
            return GetGridCellCoordinates(gridSize, position.X, position.Y);
        }

        /// <summary>
        /// Draw the countdown
        /// </summary>
        /// <param name="g">Graphics object</param>
        private void DrawCountdown(Graphics g)
        {
            SizeF s = g.VisibleClipBounds.Size;

            int dimension = 1 + (int)Math.Min(s.Width, s.Height) / 2;
            Point center = new Point((int)s.Width / 2, (int)s.Height / 2);
            Rectangle position = new Rectangle(center.X - dimension / 2, center.Y - dimension / 2, dimension, dimension);
            float angle = 360 - (360 * (gameStart.ElapsedMilliseconds / (float)(SECONDS_BEFORE_START * 1000)));

            g.FillPie(new SolidBrush(Color.FromArgb(50, Color.Gray)), position, -90, angle);
        }
    }
}
