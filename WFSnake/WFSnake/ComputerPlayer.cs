﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFSnake
{
    class ComputerPlayer
    {
        private Game game;
        private bool isGoingRight = true;

        public ComputerPlayer(Game game)
        {
            this.game = game;
        }

        public void Play()
        {
            Snake snake = game.Snakes[0];

            // Turn second part
            if (snake.CurrentDirection == Direction.Down)
            {
                snake.RequestDirectionChange(isGoingRight ? Direction.Left : Direction.Right);
                isGoingRight = !isGoingRight;
                return;
            }

            // Turn first part
            if (
                (isGoingRight && snake.HeadPosition.X == Game.GRID_WIDTH - 1)
                || (!isGoingRight && snake.HeadPosition.X == 0)
            )
                snake.RequestDirectionChange(Direction.Down);
        }
    }
}
