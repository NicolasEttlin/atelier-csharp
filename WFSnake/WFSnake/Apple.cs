﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace WFSnake
{
    class Apple
    {
        public Apple(Point position)
        {
            Position = position;
            Dimensions = new Size(1, 1);
        }

        /// <summary>
        /// Apple name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The apple's position in the grid (from the top left)
        /// </summary>
        public Point Position { get; private set; }

        /// <summary>
        /// Apple dimensions
        /// </summary>
        public Size Dimensions { get; set; }

        /// <summary>
        /// Apple hitbox
        /// </summary>
        public Rectangle Box
            => new Rectangle(Position, Dimensions);

        /// <summary>
        /// Apple color
        /// </summary>
        public Color Color { get; set; } = Color.DarkRed;

        /// <summary>
        /// Apple life span in game ticks (0 = infinite)
        /// </summary>
        public int LifeSpan { get; set; } = 0;
    }
}
