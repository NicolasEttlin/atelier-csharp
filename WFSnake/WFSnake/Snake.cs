﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace WFSnake
{
    public enum Direction { Up, Down, Left, Right }

    class Snake
    {
        /// <summary>
        /// Should we queue the requested direction changes ?
        /// </summary>
        public const bool DIRECTION_CHANGES_QUEUED = true;

        /// <summary>
        /// The length of the snake at the beggining of the game
        /// </summary>
        public const int DEFAULT_LENGTH = 5;

        public const int INVINCIBLE_SECONDS = 3;

        public const float CENTER_PART_RATIO = 0.8F;

        public const int KEY_SIZE = 30;
        public const int KEY_MARGIN = 5;

        private long invincibleModeOff = 0;
        private Stopwatch gameStart = Stopwatch.StartNew();
        private Queue<Direction> requestedDirectionChanges = new Queue<Direction>();

        /// <summary>
        /// Create a new Snake
        /// </summary>
        /// <param name="y">Y start position</param>
        public Snake(int y)
        {
            for (int i = 0; i < DEFAULT_LENGTH; i++)
                Elements.Add(new Point(i, y));
        }

        /// <summary>
        /// Current snake direction
        /// </summary>
        public Direction CurrentDirection { get; private set; } = Direction.Right;

        public Point Speed
        {
            get
            {
                switch (CurrentDirection)
                {
                    case Direction.Up:
                        return new Point(0, -1);
                    case Direction.Down:
                        return new Point(0, 1);
                    case Direction.Left:
                        return new Point(-1, 0);
                    case Direction.Right:
                        return new Point(1, 0);
                }

                throw new Exception("");
            }
        }

        /// <summary>
        /// The snake body parts
        /// </summary>
        public List<Point> Elements { get; set; } = new List<Point>();

        /// <summary>
        /// Get the snake head position
        /// </summary>
        public Point HeadPosition => Elements[Elements.Count - 1];

        /// <summary>
        /// The next head position of the snake
        /// </summary>
        public Point NextHeadPosition
        {
            get
            {
                int x = HeadPosition.X + Speed.X,
                    y = HeadPosition.Y + Speed.Y;

                if (x < 0)
                    x += Game.GRID_WIDTH;

                if (x >= Game.GRID_WIDTH)
                    x -= Game.GRID_WIDTH;

                if (y < 0)
                    y += Game.GRID_HEIGHT;

                if (y >= Game.GRID_HEIGHT)
                    y -= Game.GRID_HEIGHT;

                return new Point(x, y);
            }
        }

        /// <summary>
        /// Textual representation of the direction queue
        /// </summary>
        public string DirectionQueue
            => string.Join(", ", requestedDirectionChanges);

        public Color Color { get; set; } = Color.Green;

        public Keys[] Keymap { get; set; }

        public int Score { get; set; } = 0;

        public bool IsInvincible
            => gameStart.ElapsedMilliseconds < invincibleModeOff;

        /// <summary>
        /// Make the snake move
        /// </summary>
        public void Move(bool removeTail)
        {
            // Create a new point (the new snake head)
            Elements.Add(NextHeadPosition);

            // Remove the end of the snake
            if (removeTail)
                Elements.RemoveAt(0);
        }

        /// <summary>
        /// Add a direction change to the queue
        /// </summary>
        /// <param name="direction">New direction</param>
        public void RequestDirectionChange(Direction direction)
        {
            if (!DIRECTION_CHANGES_QUEUED)
                requestedDirectionChanges.Clear();

            requestedDirectionChanges.Enqueue(direction);
        }

        /// <summary>
        /// Perform the first possible direction change in the queue
        /// </summary>
        public void PerformDirectionChange()
        {
            while (requestedDirectionChanges.Count > 0)
            {
                Direction d = requestedDirectionChanges.Dequeue();

                if (ChangeDirection(d)) // The change was effective
                {
                    return;
                }
            }
        }

        /// <summary>
        /// Method called when the snake is hurt
        /// </summary>
        public void Hurt()
        {
            invincibleModeOff = gameStart.ElapsedMilliseconds + INVINCIBLE_SECONDS * 1000;

            // Restore the default length
            Elements.RemoveRange(0, Elements.Count - DEFAULT_LENGTH);
        }

        /// <summary>
        /// Handle a key press
        /// </summary>
        /// <param name="keyCode">The pressed key</param>
        /// <returns>True if the event was handled</returns>
        public bool HandleKeyPress(Keys keyCode)
        {
            if (Keymap == null)
                return false;

            Direction[] directions = { Direction.Up, Direction.Left, Direction.Down, Direction.Right };

            for (int i = 0; i < directions.Length; i++)
                if (Keymap[i] == keyCode)
                {
                    RequestDirectionChange(directions[i]);
                    return true;
                }

            return false;
        }

        /// <summary>
        /// Draw a snake part
        /// </summary>
        /// <param name="g">Graphics object</param>
        /// <param name="rectangle">Rectangle where the part will be drawn</param>
        /// <param name="index">Index of the part</param>
        public void DrawElement(Graphics g, RectangleF rectangle, int index)
        {
            Brush b = new SolidBrush(Color);

            if (IsInvincible)
                b = new SolidBrush(Color.FromArgb(50, Color));

            rectangle.Inflate(1F, 1F); // Avoid rounding issues
            PointF position = rectangle.Location;
            SizeF size = rectangle.Size;

            Point currentElement = Elements[index]; // Current element position in the game grid

            List<Point> adjacentElements = new List<Point>();

            if (index > 0) // Add previous element
                adjacentElements.Add(Elements[index - 1]);

            if (index < Elements.Count - 1) // Add next element
                adjacentElements.Add(Elements[index + 1]);

            adjacentElements.Add(currentElement);

            // Center
            float cornerPartRatio = 1.0F - CENTER_PART_RATIO;
            SizeF borderSize = new SizeF(cornerPartRatio * size.Width / 2, cornerPartRatio * size.Height / 2);
            PointF bottomRight = new PointF(position.X + size.Width, position.Y + size.Height);

            RectangleF fullCell = new RectangleF(position, size);
            RectangleF center = new RectangleF(position.X + borderSize.Width, position.Y + borderSize.Height, size.Width * CENTER_PART_RATIO, size.Height * CENTER_PART_RATIO);

            g.FillEllipse(b, center);

            // Element links
            foreach (Point element in adjacentElements)
            {
                Point distance = new Point((element.X - currentElement.X) % Game.GRID_WIDTH, (element.Y - currentElement.Y) % Game.GRID_HEIGHT);

                if (Math.Abs(distance.X) > 1)
                    distance = new Point(distance.X > 0 ? -1 : 1, distance.Y);

                if (Math.Abs(distance.Y) > 1)
                    distance = new Point(0, distance.Y > 0 ? -1 : 1);

                if (distance.Y == -1) // Up
                    g.FillRectangle(b, position.X + borderSize.Width, position.Y, center.Width, size.Height / 2);
                else if (distance.X == -1) // Left
                    g.FillRectangle(b, (float)Math.Floor(position.X), position.Y + borderSize.Height, (float)Math.Ceiling(size.Width / 2), center.Height);
                else if (distance.Y == 1) // Bottom
                    g.FillRectangle(b, position.X + borderSize.Width, position.Y + size.Height / 2, center.Width, size.Height / 2);
                else if (distance.X == 1) // Right
                    g.FillRectangle(b, (float)Math.Floor(position.X + size.Width / 2), position.Y + borderSize.Height, (float)Math.Ceiling(size.Width / 2), center.Height);
            }
        }

        /// <summary>
        /// Draw the keymap
        /// </summary>
        /// <param name="g"></param>
        /// <param name="position"></param>
        public void DrawKeymap(Graphics g, PointF position)
        {
            if (Keymap == null)
                return;

            DrawKey(g, new PointF(position.X + KEY_SIZE + KEY_MARGIN, position.Y), 0);
            DrawKey(g, new PointF(position.X, position.Y + KEY_SIZE + KEY_MARGIN), 1);
            DrawKey(g, new PointF(position.X + KEY_SIZE + KEY_MARGIN, position.Y + KEY_SIZE + KEY_MARGIN), 2);
            DrawKey(g, new PointF(position.X + 2 * (KEY_SIZE + KEY_MARGIN), position.Y + KEY_SIZE + KEY_MARGIN), 3);
        }

        /// <summary>
        /// Draw a specific key
        /// </summary>
        /// <param name="g">Graphics object</param>
        /// <param name="position">Key position</param>
        private void DrawKey(Graphics g, PointF position, int index)
        {
            Keys key = Keymap[index];

            string symbol = (Keymap[0] != Keys.Up) ? key.ToString() : "↑←↓→"[index].ToString();
            if (Keymap[0] == Keys.NumPad8)
                symbol = symbol[symbol.Length - 1].ToString();

            g.DrawRectangle(Pens.Gray, position.X, position.Y, KEY_SIZE, KEY_SIZE);
            g.DrawString(symbol, new Font("Arial", 16), Brushes.Black, position);
        }

        /// <summary>
        /// Change the direction of the snake
        /// </summary>
        /// <param name="newDirection">The new direction</param>
        /// <returns>True if the direction was changed; false if it wasn't</returns>
        private bool ChangeDirection(Direction newDirection)
        {
            if ( // Prevent the player from doing an U-turn
                newDirection == Direction.Right && CurrentDirection == Direction.Left
                || newDirection == Direction.Left && CurrentDirection == Direction.Right
                || newDirection == Direction.Up && CurrentDirection == Direction.Down
                || newDirection == Direction.Down && CurrentDirection == Direction.Up
            )
                return false;

            CurrentDirection = newDirection;
            return true;
        }

    }
}
