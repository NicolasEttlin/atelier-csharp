﻿namespace WFSnake
{
    partial class frmTitle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnPlayers1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnPlayers3 = new System.Windows.Forms.Button();
            this.btnPlayers2 = new System.Windows.Forms.Button();
            this.btnPlayers4 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tmrLabels = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPlayers1
            // 
            this.btnPlayers1.BackColor = System.Drawing.Color.White;
            this.btnPlayers1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPlayers1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPlayers1.FlatAppearance.BorderSize = 0;
            this.btnPlayers1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnPlayers1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlayers1.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlayers1.Location = new System.Drawing.Point(30, 30);
            this.btnPlayers1.Margin = new System.Windows.Forms.Padding(30);
            this.btnPlayers1.Name = "btnPlayers1";
            this.btnPlayers1.Size = new System.Drawing.Size(138, 128);
            this.btnPlayers1.TabIndex = 0;
            this.btnPlayers1.Text = "1";
            this.btnPlayers1.UseVisualStyleBackColor = false;
            this.btnPlayers1.Click += new System.EventHandler(this.btnPlayers_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(801, 388);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.Controls.Add(this.btnPlayers3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnPlayers2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnPlayers4, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnPlayers1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 197);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(795, 188);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // btnPlayers3
            // 
            this.btnPlayers3.BackColor = System.Drawing.Color.White;
            this.btnPlayers3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPlayers3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPlayers3.FlatAppearance.BorderSize = 0;
            this.btnPlayers3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnPlayers3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlayers3.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlayers3.Location = new System.Drawing.Point(426, 30);
            this.btnPlayers3.Margin = new System.Windows.Forms.Padding(30);
            this.btnPlayers3.Name = "btnPlayers3";
            this.btnPlayers3.Size = new System.Drawing.Size(138, 128);
            this.btnPlayers3.TabIndex = 2;
            this.btnPlayers3.Text = "3";
            this.btnPlayers3.UseVisualStyleBackColor = false;
            this.btnPlayers3.Click += new System.EventHandler(this.btnPlayers_Click);
            // 
            // btnPlayers2
            // 
            this.btnPlayers2.BackColor = System.Drawing.Color.White;
            this.btnPlayers2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPlayers2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPlayers2.FlatAppearance.BorderSize = 0;
            this.btnPlayers2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnPlayers2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlayers2.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlayers2.Location = new System.Drawing.Point(228, 30);
            this.btnPlayers2.Margin = new System.Windows.Forms.Padding(30);
            this.btnPlayers2.Name = "btnPlayers2";
            this.btnPlayers2.Size = new System.Drawing.Size(138, 128);
            this.btnPlayers2.TabIndex = 1;
            this.btnPlayers2.Text = "2";
            this.btnPlayers2.UseVisualStyleBackColor = false;
            this.btnPlayers2.Click += new System.EventHandler(this.btnPlayers_Click);
            // 
            // btnPlayers4
            // 
            this.btnPlayers4.BackColor = System.Drawing.Color.White;
            this.btnPlayers4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPlayers4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPlayers4.FlatAppearance.BorderSize = 0;
            this.btnPlayers4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnPlayers4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlayers4.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlayers4.Location = new System.Drawing.Point(624, 30);
            this.btnPlayers4.Margin = new System.Windows.Forms.Padding(30);
            this.btnPlayers4.Name = "btnPlayers4";
            this.btnPlayers4.Size = new System.Drawing.Size(141, 128);
            this.btnPlayers4.TabIndex = 3;
            this.btnPlayers4.Text = "4";
            this.btnPlayers4.UseVisualStyleBackColor = false;
            this.btnPlayers4.Click += new System.EventHandler(this.btnPlayers_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(795, 194);
            this.label1.TabIndex = 0;
            this.label1.Text = "snek";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tmrLabels
            // 
            this.tmrLabels.Enabled = true;
            this.tmrLabels.Interval = 10;
            this.tmrLabels.Tick += new System.EventHandler(this.tmrLabels_Tick);
            // 
            // frmTitle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(801, 388);
            this.Controls.Add(this.tableLayoutPanel1);
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(500, 400);
            this.Name = "frmTitle";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Snek";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmTitle_KeyPress);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPlayers1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnPlayers3;
        private System.Windows.Forms.Button btnPlayers2;
        private System.Windows.Forms.Button btnPlayers4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer tmrLabels;
    }
}