﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace WFSnake
{
    public partial class frmTitle : Form
    {
        Random rnd = new Random();
        List<TitleScreenLabel> labels = new List<TitleScreenLabel>();

        /// <summary>
        /// Prob : 1 / N
        /// </summary>
        const int LABEL_APPARITION_ODDS = 100;

        public frmTitle()
        {
            InitializeComponent();
        }

        private void StartNewGame(int playersCount)
        {
            frmGame game = new frmGame(playersCount);
            game.Show();
            Hide();

            game.FormClosed += Game_FormClosed;
        }

        private void Game_FormClosed(object sender, FormClosedEventArgs e)
        {
            Show();
        }

        private void btnPlayers_Click(object sender, EventArgs e)
        {
            StartNewGame(Convert.ToInt32((sender as Button).Text));
        }

        private void frmTitle_KeyPress(object sender, KeyPressEventArgs e)
        {
            int numberPressed;

            if (int.TryParse(e.KeyChar.ToString(), out numberPressed) && numberPressed >= 1 && numberPressed <= 4)
                StartNewGame(numberPressed);
        }

        /// <summary>
        /// Generate new labels
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmrLabels_Tick(object sender, EventArgs e)
        {
            // Spawn new labels
            if (rnd.Next(LABEL_APPARITION_ODDS) == 0)
            {
                TitleScreenLabel tsl = new TitleScreenLabel(rnd);
                labels.Add(tsl);
                Controls.Add(tsl);
                tsl.BringToFront();
            }

            foreach (TitleScreenLabel l in labels)
                l.UpdateStatus();

            foreach (TitleScreenLabel l in labels)
                l.UpdateStatus();

            labels.RemoveAll(l => l.Obsolete);
        }
    }
}
