﻿using System;
using System.Drawing;
using System.Collections.Generic;
using WFMovingObject;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SpaceInvadersTests
{
    [TestClass]
    public class GeometryTests
    {
        [TestMethod]
        public void TestComputeSimpleDistance()
        {
            PointF a = new PointF(0, 0);
            PointF b = new PointF(1, 0);
            
            Assert.AreEqual(1, Geometry.Distance(a, b));

            PointF c = new PointF(3, 4);
            Assert.AreEqual(5, Geometry.Distance(a, c));
        }

        [TestMethod]
        public void TestComputePathLength()
        {
            List<PointF> path = new List<PointF> {
                new PointF(0, 0),
                new PointF(3, 4),
            };
            Assert.AreEqual(10, Geometry.TotalPathLength(path));

            List<PointF> path1 = new List<PointF> {
                new PointF(0, 0),
                new PointF(3, 4),
                new PointF(0, 0),
                new PointF(3, 4),
            };
            Assert.AreEqual(20, Geometry.TotalPathLength(path1));

            List<PointF> path2 = new List<PointF> {
                new PointF(0, 0),
                new PointF(1, 0),
                new PointF(0, 0),
                new PointF(0, 1),
            };
            Assert.AreEqual(4, Geometry.TotalPathLength(path2));
        }
    }
}
