﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Chess.Models
{
    public enum Team { White, Black };

    public enum GameState { Ongoing, WhiteWins, BlackWins, Stalemate };

	public class Chessboard : ICloneable
    {
        public const int HEIGHT = 8;
        public const int WIDTH = 8;

		public const string DEFAULT_LAYOUT_FEN = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

        public List<Piece> Pieces { get; private set; } = new List<Piece>();

        public Team Turn { get; set; } = Team.White;

		public GameState State { get; private set; } = GameState.Ongoing;

        public List<Move> History { get; private set; } = new List<Move>();

        /// <summary>
        /// Creates a new game instance with the default position
        /// </summary>
		public Chessboard() : this(DEFAULT_LAYOUT_FEN)
		{}

        /// <summary>
        /// Creates a game from a particular board position
        /// </summary>
		/// <param name="fen">Forsyth–Edwards Notation</param>      
        public Chessboard(string fen)
        {
            string[] fields = fen.Split(' ');

            if (fields.Length < 4)
                throw new ArgumentException("FEN invalide : pas assez de champs");

            string layout = fields[0];
            string active = fields[1];
            string castling = fields[2];
            string enPassant = fields[3];
            // string halfmoves = fields[4]; // unused for now 
            // string fullmoves = fields[5];

            // Layout
            string[] ranks = layout.Split('/');

            if (ranks.Length != HEIGHT)
                throw new ArgumentException("FEN invalide : pas le bon nombre de rangs");

            for (int y = 0; y < HEIGHT; y++)
            {
                int x = 0;

                foreach (char c in ranks[y])
                {
                    int skipCount;
                    if (int.TryParse(c.ToString(), out skipCount))
                        x += skipCount;
                    else
                    {
                        Dictionary<char, PieceType> types = new Dictionary<char, PieceType>
                        {
                            ['P'] = PieceType.Pawn,
                            ['N'] = PieceType.Knight,
                            ['K'] = PieceType.King,
                            ['Q'] = PieceType.Queen,
                            ['B'] = PieceType.Bishop,
                            ['R'] = PieceType.Rook,
                        };

                        char typeAbbr = char.ToUpper(c);
                        PieceType type = types[typeAbbr];
                        Team team = typeAbbr == c ? Team.White : Team.Black;

                        Pieces.Add(new Piece(type, team, new Point(x, y)));

                        x += 1;
                    }

                }
            }

            // Turn
            if (active == "b")
                Turn = Team.Black;
            else if (active == "w")
                Turn = Team.White;
            else
                throw new ArgumentException("FEN invalide : équipe actuelle non valide");

            // En passant
            if (enPassant != "-")
            {
                Point point = PointFromCoordinates(enPassant);
                Piece enPassantPiece = new Piece(PieceType.Pawn, point.Y == 2 ? Team.Black : Team.White, point);
                enPassantPiece.Virtual = true;
                Pieces.Add(enPassantPiece);
            }

            // Castling
            foreach (Piece p in Pieces.Where(p => p.Type == PieceType.Rook))
                p.WasMoved = true; // Default : disable castling on all rooks pieces
            
            foreach (char side in "KQkq")
            {
                Team team = side.IsLowercase() ? Team.Black : Team.White;
                if (castling.Contains(side))
                {
                    Piece rook = Pieces
                        .Where(p => p.Team == team && p.Type == PieceType.Rook)
                        .Where(p => p.Position.Y == (p.Team == Team.Black ? 0 : HEIGHT - 1))
                        .Where(p => p.Position.X == (Char.ToUpper(side) == 'Q' ? 0 : WIDTH - 1))
                        .First();

                    rook.WasMoved = false;
                }
            }
        }
        
        /// <summary>
        /// Gets the piece located on a specific square
        /// </summary>
		/// <returns>The piece object, or null if no piece exists there</returns>
        /// <param name="coordinates">Square coordinates</param>
		public Piece GetPieceAt(Point coordinates)
		{
			return Pieces.Find(p => p.Position == coordinates);
		}

        /// <summary>
        /// Get all the squares in the game
        /// </summary>
        public List<Point> GetAllSquares()
		{
			List<Point> squares = new List<Point>();

			for (int i = 0; i < Chessboard.WIDTH * Chessboard.HEIGHT; i += 1)
                squares.Add(new Point(i % Chessboard.WIDTH, i / Chessboard.HEIGHT));

			return squares;
		}

        /// <summary>
        /// Plays a move on the chessboard
        /// </summary>
        /// <param name="move">The move to play</param>
        public void PlayMove(Move move)
        {
            Piece piece = GetPieceAt(move.From);

            if (piece == null)
                throw new ArgumentException("No piece found on the initial position");

            MovePiece(piece, move.To);

            if (move.Promotion.HasValue)
                piece.PromoteTo(move.Promotion.Value);
        }

        /// <summary>
        /// Move a piece to a new position
        /// </summary>
		/// <param name="piece">The piece to move</param>
        /// <param name="newPosition">The new position of the piece</param>
        public void MovePiece(Piece piece, Point newPosition)
        {
			if (piece.Team != Turn)
				throw new Exception("This piece cannot be moved now.");
            
			if (!piece.GetPossibleNextPositions(this, false).Contains(newPosition))
				throw new Exception("This move cannot be played.");

            Point oldPosition = piece.Position;
            
			// Capture
			Piece pieceToEat = GetPieceAt(newPosition);         
			if (pieceToEat != null)
			{
				Pieces.Remove(pieceToEat);

				// “En passant” : remove real pawn
				if (piece.Type == PieceType.Pawn && pieceToEat.Virtual == true)
					Pieces.Remove(Pieces.Find(p => p.Position == pieceToEat.Position + new Size(0, pieceToEat.Heading)));
			}

			piece.Position = newPosition;

            // Castling
            if (piece.Type == PieceType.King && Math.Abs(oldPosition.X - newPosition.X) == 2)
            {
                bool isBigCastling = oldPosition.X - newPosition.X > 0; // Going left
                int currentRookX = isBigCastling ? 0 : 7;

                Piece king = piece;
                Piece rook = GetPieceAt(new Point(currentRookX, king.Position.Y));
                rook.Position = new Point(king.Position.X + (isBigCastling ? 1 : -1), rook.Position.Y); 
				rook.WasMoved = true;
            }

			// Add “en passant” pawn
			if (piece.Type == PieceType.Pawn && Math.Abs(newPosition.Y - oldPosition.Y) == 2)
			{
				Piece virtualPiece = new Piece(PieceType.Pawn, piece.Team, oldPosition + new Size(0, piece.Heading));
				virtualPiece.Virtual = true;

				Pieces.Add(virtualPiece);
			}
            
			// Remove old “en passant” virtual points
			Pieces.RemoveAll(p => p.Virtual && p.Team != Turn);
            piece.Position = newPosition;

            piece.WasMoved = true;

            History.Add(new Move(oldPosition, newPosition));

            Turn = Turn.Enemy();
        }

        /// <summary>
		/// Know if a team is in check (the other team could eat the king)
        /// </summary>
        /// <returns>True if the team is in check, false otherwise</returns>
        /// <param name="team">The team of the king that could be in check</param>
		public bool IsTeamInCheck(Team team)
		{
			Piece myTeamKing = Pieces.Where(p => p.Type == PieceType.King && p.Team == team).First();
			var piecesOfEnemyTeam = Pieces.Where(p => p.Team != team);

			foreach (Piece enemyPiece in piecesOfEnemyTeam)
				if (enemyPiece.GetPossibleNextPositions(this, false).Contains(myTeamKing.Position))
					return true;

			return false;
		}

		public string ToFEN()
		{
			string fen = "";
            
            // Layout
			for (int y = 0; y < HEIGHT; y++)
			{
				int emptySquares = 0;

				if (fen != string.Empty)
					fen += "/";

				for (int x = 0; x < WIDTH; x += 1)
				{
					Piece piece = GetPieceAt(new Point(x, y));

					if (piece == null || piece.Virtual == true)
					{
						emptySquares += 1;
					}
					else
					{
						if (emptySquares != 0)
						{
							fen += emptySquares;
                            emptySquares = 0;
						}

						char pieceType = piece.Type.Abbreviation();

						if (piece.Team == Team.Black)
							pieceType = Char.ToLower(pieceType);

						fen += pieceType;
					}
				}

				// Remaining empty squares on the line
				if (emptySquares != 0)
				    fen += emptySquares;
			}

            // Turn
			fen += ' ';
			fen += Turn == Team.Black ? 'b' : 'w';

            // Castling (@TODO)
            string availableCastlings = string.Empty;

            foreach (Team team in new Team[] { Team.White, Team.Black })
            {
                // The team's king must have not moved
                Piece king = Pieces
                    .Where(p => p.Type == PieceType.King)
                    .Where(p => p.Team == team)
                    .First();
                if (king.WasMoved) continue;

                IEnumerable<Piece> rooksWhoCanCastle = Pieces
                    .Where(p => p.Type == PieceType.Rook)
                    .Where(p => p.Team == team)
                    .Where(p => p.WasMoved == false)
                    .OrderByDescending(p => p.Position.X); // First 'K', then 'Q'

                foreach (Piece rook in rooksWhoCanCastle)
                {
                    char sideName = rook.Position.X == 0 ? 'q' : 'k';
                    availableCastlings += team == Team.White ? Char.ToUpper(sideName) : sideName;
                }
            }

            if (availableCastlings == string.Empty)
                availableCastlings = "-";
            fen += $" {availableCastlings}";
            
			// En passant
			fen += ' ';
			Piece enPassantPiece = Pieces.Where(p => p.Virtual == true).FirstOrDefault();
			if (enPassantPiece == null)
				fen += '-';
			else
				fen += enPassantPiece.Position.Coordinates();
         
			return fen;
		}

        /// <summary>
        /// Updates the game state
        /// </summary>
        public void UpdateState()
		{
			if (State != GameState.Ongoing) return;

            // White can’t move
			if (Turn == Team.White && Pieces.Where(p => p.Team == Turn).All(p => p.GetPossibleNextPositions(this).Count == 0))
				State = IsTeamInCheck(Turn) ? GameState.BlackWins : GameState.Stalemate;

            // Black can’t move
			if (Turn == Team.Black && Pieces.Where(p => p.Team == Turn).All(p => p.GetPossibleNextPositions(this).Count == 0))
				State = IsTeamInCheck(Turn) ? GameState.WhiteWins : GameState.Stalemate;
		}

        /// <summary>
        /// Creates a copy of the chessboard (useful to test hypotetical scenarios)
        /// </summary>
        /// <returns>A deep copy of the chessboard instance</returns>
        public object Clone()
        {
            Chessboard clone = (Chessboard)MemberwiseClone();

            // Deep copy the lists
            clone.Pieces = clone.Pieces.Select(p => p.Clone() as Piece).ToList();
            clone.History = clone.History.Select(m => m.Clone() as Move).ToList();

            return clone;
        }

        /// <summary>
        /// Returns a new Point from chess-style coordinates
        /// </summary>
        /// <returns>Point</returns>
		/// <param name="coordinates">Coordinates (example : 'd4')</param>
        private Point PointFromCoordinates(string coordinates)
		{
			coordinates = coordinates.ToLower();

			if (coordinates.Length != 2)
				throw new FormatException();
			
			char column = coordinates[0];
			char row = coordinates[1];

			int x = column - 97;
			int y = 8 - Convert.ToInt32(row.ToString());

			if (x < 0 || y < 0 || x > WIDTH - 1 || y > HEIGHT - 1)
				throw new ArgumentException();

			return new Point(x, y);
		}
	}
}
