﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Models
{
    /// <summary>
    /// Move in the game history
    /// </summary>
    public class Move : ICloneable
    {
        /// <summary>
        /// Initial square of the piece
        /// </summary>
        public Point From { get; private set; }

        /// <summary>
        /// New square of the piece
        /// </summary>
        public Point To { get; private set; }

        /// <summary>
        /// New type of the piece (optional)
        /// </summary>
        public PieceType? Promotion { get; set; } = null;

        /// <summary>
        /// Comment on the move (optional)
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Creates a new move
        /// </summary>
        /// <param name="from">Initial piece square coordinates</param>
        /// <param name="to">New piece square coordinates</param>
        public Move(Point from, Point to)
        {
            From = from;
            To = to;
        }

        /// <summary>
        /// Clone the move object
        /// </summary>
        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
