﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Models
{
    public enum PieceType { Pawn, Knight, Bishop, Rook, Queen, King }

    public class Piece : ICloneable
    {
        public PieceType Type { get; private set; }

        public Team Team { get; private set; }

        public Point Position { get; set; }

        public string Name { get; }

        /// <summary>
        /// Know if the piece was moved (= can still castle)
        /// </summary>
        public bool WasMoved { get; set; } = false;

		/// Is the piece “virtual”? (it exists only for a turn so it can be captured ‘en passant’)
        /// </summary>
		public bool Virtual { get; set; } = false;

        /// <summary>
		/// The “heading” of the piece (Y direction of the piece)
        /// </summary>
		public int Heading
		    => Team == Team.Black ? 1 : -1;

        public Piece(PieceType type, Team team, Point position)
        {
            Type = type;
            Team = team;
            Position = position;
        }

        /// <summary>
        /// Get all the possible next positions
        /// </summary>
        /// <param name="chessboard">The current state of the game</param>
        /// <returns>List of all the positions where the piece can go</returns>
		public List<Point> GetPossibleNextPositions(Chessboard chessboard, bool withoutIllegalMoves = true)
        {
            List<Point> positions = new List<Point>();

            if (Type == PieceType.King) // King
            {
                positions.AddRange(
                    new List<Point> {
                        new Point(Position.X + 1, Position.Y),
                        new Point(Position.X - 1, Position.Y),
                        new Point(Position.X, Position.Y + 1),
                        new Point(Position.X, Position.Y - 1),
                        new Point(Position.X + 1, Position.Y + 1),
                        new Point(Position.X - 1, Position.Y - 1),
                        new Point(Position.X + 1, Position.Y - 1),
                        new Point(Position.X - 1, Position.Y + 1)
                    }
                    .Where(s => IsValidLocation(s, chessboard))
                );

                // Castelling
                if (!WasMoved)
                {
                    IEnumerable<Piece> rooks = chessboard.Pieces
                        .Where(p => p.Type == PieceType.Rook)
                        .Where(p => p.Team == Team)
                        .Where(p => p.WasMoved == false);

                    foreach (Piece rook in rooks)
                    {
                        // Are there pieces between the rook and the king?
                        int directionOfRook = rook.Position.X < Position.X ? -1 : 1;

                        if (!AreTherePiecesBetween(this, rook, new Size(directionOfRook, 0), chessboard))
                        {
                            positions.Add(
                                new Point(Position.X + 2 * directionOfRook, Position.Y)
                            );
                        }
                    }
                }
            }

            if (Type == PieceType.Knight) // Knight
                positions.AddRange(
                    new List<Point> {
                        new Point(Position.X + 1, Position.Y + 2),
                        new Point(Position.X + 2, Position.Y + 1),
                        new Point(Position.X - 1, Position.Y - 2),
                        new Point(Position.X - 2, Position.Y - 1),
                        new Point(Position.X + 2, Position.Y - 1),
                        new Point(Position.X - 1, Position.Y + 2),
                        new Point(Position.X - 2, Position.Y + 1),
                        new Point(Position.X + 1, Position.Y - 2),
                    }
                    .Where(s => IsValidLocation(s, chessboard))
                );

            /*
             * “Directions”
             */
            List<Size> directions = new List<Size>();
            if (Type == PieceType.Rook || Type == PieceType.Queen) // Neighboring
            {
                directions.AddRange(new List<Size>() {
                    new Size(1, 0),
                    new Size(-1, 0),
                    new Size(0, 1),
                    new Size(0, -1),
                });
            }

            if (Type == PieceType.Bishop || Type == PieceType.Queen) // Diagonals
            {
                directions.AddRange(new List<Size>() {
                    new Size(1, 1),
                    new Size(1, -1),
                    new Size(-1, 1),
                    new Size(-1, -1),
                });
            }

            foreach (Size direction in directions)
            {
                Point currentPosition = Position;

                while (true)
                {
                    currentPosition += direction;

                    if (!IsInGameZone(currentPosition)) break; // Out of the board

                    Piece pieceThere = chessboard.GetPieceAt(currentPosition);
                    if (pieceThere != null && pieceThere.Team == Team) // Allied piece
                        break;

                    positions.Add(currentPosition);

                    if (pieceThere != null && pieceThere.Team != Team) // Enemy piece (can’t go beyond)
                        break;
                }
            }

            if (Type == PieceType.Pawn) // Pawn
            {
                // Places where the pawn can only move if no other piece stands there
                List<Point> peacefulPositions = new List<Point>()
                {
                    Position + new Size(0, Heading) // 1 square ahead
				};

                if ((Heading == 1 ? Position.Y : Chessboard.HEIGHT - Position.Y - 1) == 1) // 2 squares ahead
                    peacefulPositions.Add(Position + new Size(0, 2 * Heading));

                foreach (Point peacefulPosition in peacefulPositions)
                    if (IsInGameZone(peacefulPosition) && chessboard.GetPieceAt(peacefulPosition) == null)
                        positions.Add(peacefulPosition);
                    else
                        break; // Don’t add the second peaceful position if the first is invalid

                // Places where the pawn can only move to attack an enemy
                List<Point> attacks = new List<Point>()
                {
                    Position + new Size(1, Heading),
                    Position + new Size(-1, Heading)
                };
                foreach (Point attack in attacks)
                {
                    Piece pieceThere = chessboard.GetPieceAt(attack);
                    if (pieceThere != null && pieceThere.Team != Team)
                        positions.Add(attack);
                }
            }

            // Remove illegal moves (leading to a check)
            if (withoutIllegalMoves)
                positions = positions.Where(p => IsLegalMove(p, chessboard)).ToList();

            return positions;
        }

        /// <summary>
        /// Promotes the piece to a new type
        /// </summary>
        /// <param name="type">The new type of the piece</param>
        public void PromoteTo(PieceType type)
        {
            Type = type;
        }

        /// <summary>
        /// Is the position valid (a piece could go there), i.e. :
        /// — Is on the chessboard
        /// — Has no piece of the same team there
        /// </summary>
        /// <param name="position">The position</param>
        /// <param name="chessboard">The game</param>
        /// <returns>Boolean</returns>
        private bool IsValidLocation(Point position, Chessboard chessboard)
        {
            if (!IsInGameZone(position)) return false;

            Piece pieceThere = chessboard.GetPieceAt(position);
            return pieceThere == null || pieceThere.Team != Team;
        }

        /// <summary>
        /// Is a coordinate on the board ?
        /// </summary>
        /// <returns>Boolean</returns>
        /// <param name="position">The position to test</param>
		private bool IsInGameZone(Point position)
        {
            Rectangle gameZone = new Rectangle(0, 0, Chessboard.WIDTH, Chessboard.HEIGHT);
            return gameZone.Contains(position);
        }

        /// <summary>
		/// Is the move a legal move? (Illegal move : move leading to a check)
        /// </summary>
        /// <returns>True if the move is legal, false for an illegal move</returns>
        /// <param name="newPosition">The new position</param>
        /// <param name="chessboard">The chessboard</param>
		private bool IsLegalMove(Point newPosition, Chessboard chessboard)
        {
            Chessboard hypotheticalChessboard = (Chessboard)chessboard.Clone();

            // Make the move
            Piece hypotheticalPiece = hypotheticalChessboard.Pieces.Find(p => p.Position == Position);
            hypotheticalChessboard.MovePiece(hypotheticalPiece, newPosition);

            return !hypotheticalChessboard.IsTeamInCheck(Team);
        }

        /// <summary>
        /// Use to check whether a castling is possible
        /// </summary>
        private bool AreTherePiecesBetween(Piece piece1, Piece piece2, Size direction, Chessboard chessboard)
        {
            Point position = piece1.Position;
            
            while (true)
            {
                position += direction;

                Piece pieceOnThisSquare = chessboard.GetPieceAt(position);

                if (pieceOnThisSquare != null)
                    return pieceOnThisSquare != piece2;
            }
        }

        /// <summary>
        /// Know a position, relative to the current position
        /// </summary>
        /// <param name="x">X to add</param>
        /// <param name="y">Y to add</param>
        /// <returns>The position</returns>
        private Point RelativePosition(int x, int y)
        {
            return new Point(Position.X + x, Position.Y + y);
        }


        /// <summary>
        /// Creates a copy of the piece
        /// </summary>
        /// <returns>The clone</returns>
		public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
