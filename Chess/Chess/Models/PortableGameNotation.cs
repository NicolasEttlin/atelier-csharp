﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Models
{
    /// <summary>
    /// PGN parser
    /// </summary>
    static class PortableGameNotation
    {
        /// <summary>
        /// Loads a game from a PGN content
        /// </summary>
        /// <param name="pgn">Portable Game Notation</param>
        /// <returns>Chessboard</returns>
        public static Chessboard Parse(string pgn)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Parses a move in the Standard Algebraic Notation (SAN)
        /// </summary>
        /// <param name="san">Move (e.g. 'Nc6')</param>
        /// <param name="chessboard">Current state of the chessboard</param>
        /// <returns>Move object</returns>
        private static Move ParseMove(string san, Chessboard chessboard)
        {
            throw new NotImplementedException();
        }
    }
}
