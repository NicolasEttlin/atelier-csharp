﻿using System;
using Chess.Models;
using System.Drawing;

namespace Chess
{
    static class Extensions
    {
        /// <summary>
        /// Get the piece type's abbeviation
        /// </summary>
        public static char Abbreviation(this PieceType type)
        {
            switch (type)
            {
                case PieceType.Pawn:
                    return 'P';
                case PieceType.Knight:
                    return 'N';
                case PieceType.Bishop:
                    return 'B';
                case PieceType.Rook:
                    return 'R';
                case PieceType.Queen:
                    return 'Q';
                case PieceType.King:
                    return 'K';
                default:
                    return '\0';
            }
        }

        /// <summary>
        /// Get the enemy of a team
        /// </summary>
        /// <param name="team">A team</param>
        /// <returns>The opposite team</returns>
        public static Team Enemy(this Team team)
            => team == Team.White ? Team.Black : Team.White;

        /// <summary>
        /// Converts a point to chess-style coordinates
        /// </summary>
		/// <returns>The coordinates (e.g. 'a8')</returns>
		/// <param name="point">Point (e.g. (X = 0, Y = 0)</param>
        public static string Coordinates(this Point point)      
			=> $"{Convert.ToChar(point.X + 97)}{(8 - point.Y).ToString()}";

        public static bool IsLowercase(this char c)
            => Char.ToUpper(c) != c;
    }
}
