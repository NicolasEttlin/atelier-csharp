﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Chess.Models;
using System.Diagnostics;

namespace Chess.Views
{
    public partial class frmGame : Form
    {
        private const int GAME_MARGIN = 10;
        private const int TOP_MARGIN = 24; // Top bar

        private Chessboard chessboard;

        private Piece selectedPiece;
        private List<Point> highlightedSquares = new List<Point>();

        private frmMovesHistory historyWindow;

        // Computed on screen size changes
        private SizeF screenSize;
        private SizeF squareSize;
        private RectangleF gameArea;

        public frmGame()
        {
            InitializeComponent();

            Chessboard = new Chessboard();
        }

        public Chessboard Chessboard
        {
            get => chessboard;
            set
            {
                chessboard = value;
                highlightedSquares.Clear();
                selectedPiece = null;
                TurnStart();
                Invalidate();
            }
        }

        /// <summary>
        /// Click on the game
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
		private void frmGame_MouseClick(object sender, MouseEventArgs e)
        {
            Point? squareClicked = GetSquareFromPixelCoordinates(e.Location);

            if (selectedPiece == null) // Select a piece
            {
                if (squareClicked.HasValue) // Clicked on a square
                {
                    Piece clickedPiece = Chessboard.GetPieceAt(squareClicked.Value);

                    if (clickedPiece == null || clickedPiece.Team == Chessboard.Turn)
                        selectedPiece = clickedPiece;
                    else
                        selectedPiece = null;
                }
                else // Clicked outside of a square
                    selectedPiece = null;

                if (selectedPiece != null && selectedPiece.Team == Chessboard.Turn) // Show possible next positions
                    highlightedSquares = selectedPiece.GetPossibleNextPositions(Chessboard);
                else // Unselect a piece
                    highlightedSquares.Clear();
            }
            else // Make a move
            {
                if (
                    squareClicked.HasValue
                    && squareClicked.Value != selectedPiece.Position
                    && selectedPiece.GetPossibleNextPositions(Chessboard).Contains(squareClicked.Value) // Can move there
                )
                {
                    // Play the move
                    Chessboard.MovePiece(selectedPiece, squareClicked.Value);
                }

                selectedPiece = null;
                highlightedSquares.Clear();
            }

            Invalidate();
            TurnStart();
        }

        /// <summary>
        /// Draw the game
        /// </summary>
        private void frmGame_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            e.Graphics.Clear(Color.White);

            // On screen size changes
            SizeF newSize = e.Graphics.VisibleClipBounds.Size;
            if (newSize != screenSize)
            {
                screenSize = newSize;

                // Game area
                PointF center = new PointF(screenSize.Width / 2, TOP_MARGIN + screenSize.Height / 2);
                int dimension = Math.Min((int)screenSize.Width, (int)screenSize.Height) - 2 * GAME_MARGIN;
                PointF position = new PointF(center.X - dimension / 2, center.Y - dimension / 2);
                gameArea = new RectangleF(position, new SizeF(dimension, dimension));

                // Square size
                squareSize = new SizeF(gameArea.Width / Chessboard.WIDTH, gameArea.Height / Chessboard.HEIGHT);
            }

            // Draw squares
            for (int i = 0; i < Chessboard.WIDTH * Chessboard.HEIGHT; i += 1)
            {
                Point casePosition = new Point(i % Chessboard.WIDTH, i / Chessboard.HEIGHT);
                RectangleF squareBounds = GetSquareBounds(casePosition);
                bool isSquareBlack = IsSquareBlack(casePosition);

                e.Graphics.FillRectangle(isSquareBlack ? Brushes.SaddleBrown : Brushes.Wheat, squareBounds);
            }

            // Draw pieces
            foreach (Piece piece in Chessboard.Pieces.Where(p => p.Virtual == false))
            {
                RectangleF piecePosition = GetSquareBounds(piece.Position);

                string imageName = piece.Type.Abbreviation().ToString() + (piece.Team == Team.Black ? 'B' : 'W');
                Image image = (Image)Properties.Resources.ResourceManager.GetObject(imageName);

                e.Graphics.DrawImage(image, piecePosition);
            }

            // Draw highlighted squares
            foreach (Point casePosition in highlightedSquares)
            {
                RectangleF squareBounds = GetSquareBounds(casePosition);

                float circleSize = squareBounds.Width / 2;

                Color highlightColor = Color.FromArgb(100, Color.White);
                Color highlightBorder = Color.FromArgb(150, Color.Black);

                e.Graphics.DrawEllipse(new Pen(highlightBorder, 2), new RectangleF(squareBounds.X + (squareBounds.Width - circleSize) / 2, squareBounds.Y + (squareBounds.Height - circleSize) / 2, circleSize, circleSize));
                e.Graphics.FillEllipse(new SolidBrush(highlightColor), new RectangleF(squareBounds.X + (squareBounds.Width - circleSize) / 2, squareBounds.Y + (squareBounds.Height - circleSize) / 2, circleSize, circleSize));
            }
        }

        private void tsmLoadFEN_Click(object sender, EventArgs e)
        {
            frmLoadFEN fenDialog = new frmLoadFEN();
            fenDialog.GameContent = Chessboard.ToFEN();
            fenDialog.ShowDialog();

            try
            {
                if (fenDialog.DialogResult == DialogResult.OK)
                {
                    Chessboard = new Chessboard(fenDialog.GameContent);
                }
            }
            catch (ArgumentException fenError)
            {
                DialogResult choice = MessageBox.Show(fenError.Message, "Erreur", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                if (choice == DialogResult.Retry)
                    tsmLoadFEN_Click(sender, e);
            }
        }

        private void tsmLoadPGN_Click(object sender, EventArgs e)
        {
            frmLoadPGN pgnDialog = new frmLoadPGN();
            pgnDialog.ShowDialog();

            try
            {
                if (pgnDialog.DialogResult == DialogResult.OK)
                {
                    Chessboard = PortableGameNotation.Parse(pgnDialog.GameContent);
                }
            }
            catch (ArgumentException pgnError)
            {
                DialogResult choice = MessageBox.Show(pgnError.Message, "Erreur", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                if (choice == DialogResult.Retry)
                    tsmLoadPGN_Click(sender, e);
            }
        }

        private void tsmReset_Click(object sender, EventArgs e)
        {
            Chessboard = new Chessboard();
        }

        private void tsmOpenHistory_Click(object sender, EventArgs e)
        {
            if (historyWindow == null || historyWindow.IsDisposed)
                historyWindow = new frmMovesHistory(chessboard);

            historyWindow.Show();
            historyWindow.Focus();
        }

        /// <summary>
        /// Actions made at the beginning of a turn
        /// </summary>
        private void TurnStart()
        {
            // Pawn promotion UI
            IEnumerable<Piece> promotedPawns = Chessboard.Pieces
                .Where(p => p.Type == PieceType.Pawn)
                .Where(p => p.Position.Y == 0 || p.Position.Y == Chessboard.HEIGHT - 1);

            foreach (Piece pawn in promotedPawns)
            {
                frmPromotion promotionForm = new frmPromotion(pawn);
                promotionForm.ShowDialog();
                pawn.PromoteTo(promotionForm.ChosenType);

                // Edit the last move to add the promotion
                Move lastMove = chessboard.History[chessboard.History.Count - 1];
                lastMove.Promotion = promotionForm.ChosenType;

                Invalidate();
            }

            // Update the game state
            string currentAction;

            Chessboard.UpdateState();

            if (Chessboard.State == GameState.Stalemate)
                currentAction = "Pat";
            else if (Chessboard.State == GameState.BlackWins)
                currentAction = "Victoire des noirs";
            else if (Chessboard.State == GameState.WhiteWins)
                currentAction = "Victoire des blancs";
            else
                currentAction = string.Format("C’est au tour des {0}", Chessboard.Turn == Team.White ? "blancs" : "noirs");

            // Update the title text
            Text = string.Format($"Échecs • {currentAction}");

            // Update the history window
            if (historyWindow != null && !historyWindow.IsDisposed)
                historyWindow.RefreshItems();
        }

        /// <summary>
        /// Gets the color of a square
        /// </summary>
        /// <returns>True for a black square, false for a white square</returns>
        /// <param name="position">The square coordinates</param>
        private bool IsSquareBlack(Point position)
        {
            return (position.X % 2 == (position.Y % 2 == 0 ? 1 : 0));
        }

        /// <summary>
        /// Gets the position and size of a square on the screen
        /// </summary>
        /// <returns>The square bounds.</returns>
        /// <param name="position">The square coordinates</param>
        private RectangleF GetSquareBounds(Point position)
        {
            return new RectangleF(gameArea.X + squareSize.Width * position.X, gameArea.Y + squareSize.Height * position.Y, squareSize.Width, squareSize.Height);
        }

        /// <summary>
        /// Gets the square displaying at a location on the screen
        /// </summary>
        /// <returns>The square coordinates, or null if no square exists at this location</returns>
        /// <param name="coordinates">Location on the screen</param>
        private Point? GetSquareFromPixelCoordinates(Point coordinates)
        {
            if (!gameArea.Contains(coordinates)) return null;

            PointF locationOnChessboard = new PointF()
            {
                X = coordinates.X - gameArea.X,
                Y = coordinates.Y - gameArea.Y,
            };

            return new Point()
            {
                X = (int)(locationOnChessboard.X / squareSize.Width),
                Y = (int)(locationOnChessboard.Y / squareSize.Height),
            };
        }

        private void frmGame_Resize(object sender, EventArgs e)
        {
            Invalidate();
        }
    }
}
