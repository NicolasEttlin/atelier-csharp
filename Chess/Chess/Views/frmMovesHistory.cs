﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Chess.Models;

namespace Chess.Views
{
    public partial class frmMovesHistory : Form
    {
        private List<Move> movesHistory;

        public frmMovesHistory(Chessboard chessboard)
        {
            movesHistory = chessboard.History;

            InitializeComponent();
            RefreshItems();
        }

        public void RefreshItems()
        {
            lsvMoves.Items.Clear();

            int i = 0;

            foreach (Move move in movesHistory)
            {
                lsvMoves.Items.Add(new ListViewItem(new string[] {
                    i % 2 == 0 ? (i / 2 + 1).ToString() : "",
                    move.From.Coordinates(),
                    move.To.Coordinates(),
                    move.Promotion.HasValue ? move.Promotion.Value.Abbreviation().ToString() : "",
                    move.Comment != null ? move.Comment : "",
                }));

                i += 1;
            }
        }
    }
}
