﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chess.Views
{
    public partial class frmLoadFEN : Form
    {
        public string GameContent
        {
            get => tbxFen.Text;
            set { tbxFen.Text = value; }
        }

        public frmLoadFEN()
        {
            InitializeComponent();
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
