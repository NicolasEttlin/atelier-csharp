﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Chess.Models;

namespace Chess.Views
{
    public partial class frmPromotion : Form
    {
        private Piece pawn;

        public PieceType ChosenType { get; private set; } = PieceType.Queen;

        public frmPromotion(Piece pawn)
        {
            this.pawn = pawn;

            InitializeComponent();

            PieceType[] availableTypes = {
                PieceType.Queen,
                PieceType.Knight,
                PieceType.Rook,
                PieceType.Bishop,
            };

            PictureBox[] options = {
                pictureBox1,
                pictureBox2,
                pictureBox3,
                pictureBox4,
            };

            for (int i = 0; i < options.Length; i++)
            {
                PieceType type = availableTypes[i];
                string imageName = type.Abbreviation().ToString() + (pawn.Team == Team.Black ? 'B' : 'W');
                options[i].BackgroundImage = Properties.Resources.ResourceManager.GetObject(imageName) as Image;
                options[i].Tag = type;
                options[i].Click += pbxChoice_Click;
            }
        }

        private void pbxChoice_Click(object sender, EventArgs e)
        {
            ChosenType = (PieceType)(sender as PictureBox).Tag;
            Close();
        }
    }
}
