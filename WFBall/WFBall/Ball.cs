﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

namespace WFBall
{
    class Ball
    {
        /// <summary>
        /// Ball size
        /// </summary>
        const int SIZE = 100;

        /// <summary>
        /// Current ball position
        /// </summary>
        public PointF Position { get; set; } = new PointF(100, 100);

        /// <summary>
        /// Size of the "box" the ball must stay within
        /// </summary>
        public SizeF BounceBox { get; set; }

        /// <summary>
        /// Radius of the ball
        /// </summary>
        public float Radius => SIZE / 2;

        // Points added to the ball position (each second)
        private float _speedX = -100;
        private float _speedY = -100;

        private Stopwatch _sw;
        private long _lastElapsedTime = 0;

        private long DeltaTempsMs
        {
            get
            {
                long delta = _sw.ElapsedMilliseconds - _lastElapsedTime;
                _lastElapsedTime += delta;
                return delta;
            }
        }

        public Ball()
        {
            _sw = Stopwatch.StartNew();
        }

        /// <summary>
        /// Paint the ball
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Paint(object sender, PaintEventArgs e)
        {
            // Move the ball
            long elapsedMs = DeltaTempsMs;
            Position = new PointF()
            {
                X = Position.X + (_speedX / 1000 * elapsedMs),
                Y = Position.Y + (_speedY / 1000 * elapsedMs),
            };
        

            // Bounce if needed
            if (BounceBox != null)
            {
                if (Position.X - Radius <= 0)
                    _speedX = Math.Abs(_speedX);

                if (Position.Y - Radius <= 0)
                    _speedY = Math.Abs(_speedY);

                if (Position.X + Radius >= BounceBox.Width)
                    _speedX = -1 * Math.Abs(_speedX);

                if (Position.Y + Radius >= BounceBox.Height)
                    _speedY = -1 * Math.Abs(_speedY);

                // Max and max values
                Position = new PointF() {
                    X = Math.Max(0, Math.Min(Position.X, BounceBox.Width)),
                    Y = Math.Max(0, Math.Min(Position.Y, BounceBox.Height)),
                };
            }

            // Draw the ball
            e.Graphics.FillEllipse(Brushes.Red, new RectangleF(new PointF() {
                X = Position.X - Radius,
                Y = Position.Y - Radius,
            }, new SizeF(SIZE, SIZE)));
        }
    }
}
