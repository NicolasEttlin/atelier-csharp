﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFBall
{
    public partial class frmMain : Form
    {
        Ball _ball = new Ball();

        public frmMain()
        {
            InitializeComponent();
            Paint += _ball.Paint;

            _ball.BounceBox = ClientSize;

            DoubleBuffered = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Invalidate();
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            _ball.BounceBox = ClientSize;
        }

        private void frmMain_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
        }
    }
}
