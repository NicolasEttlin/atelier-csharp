﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace WFPong
{
    class Ball : PaintableObject
    {
        public Ball()
        {
            Position = new PointF(10, 10);
            speedX = 50;
            speedY = 50;

            Size = new SizeF(100, 100);
        }

        /// <summary>
        /// Draw the ball
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.FillEllipse(Brushes.DarkViolet, new RectangleF(Position, Size));
        }
    }
}
