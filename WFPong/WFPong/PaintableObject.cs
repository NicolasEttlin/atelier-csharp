﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace WFPong
{
    abstract class PaintableObject
    {
        /// <summary>
        /// Current object position (top left coordinates)
        /// </summary>
        public PointF Position { get; set; }

        /// <summary>
        /// Object size
        /// </summary>
        public SizeF Size { get; set; }

        /// <summary>
        /// The center of the object
        /// </summary>
        public PointF Center
            => new PointF()
            {
                X = Position.X + Size.Width / 2,
                Y = Position.Y + Size.Height / 2,
            };

        /// <summary>
        /// Speed (points/second)
        /// </summary>
        protected float speedX = 0, speedY = 0;

        /// <summary>
        /// Draw the object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        abstract public void Paint(object sender, PaintEventArgs e);

        /// <summary>
        /// Move the object for a given elapsed time
        /// </summary>
        /// <param name="ms">Time elapsed</param>
        public void Move(int ms)
        {
            Position = new PointF() {
                X = Position.X + speedX * ms / 1000.0f,
                Y = Position.Y + speedY * ms / 1000.0f,
            };
        }
    }
}
