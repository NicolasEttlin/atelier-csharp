﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace WFPong
{
    class Racket : PaintableObject
    {
        public Racket(int positionX)
        {
            Position = new PointF(positionX, 0);
            Size = new SizeF(10, 100);
            speedY = 100;
        }

        public override void Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(new Pen(Color.DarkSlateGray, 2), Position.X, Position.Y, Size.Width, Size.Height);
        }
    }
}
