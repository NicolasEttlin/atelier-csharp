﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace WFPong
{
    public partial class frmMain : Form
    {
        Stopwatch time;

        List<PaintableObject> objects = new List<PaintableObject>();

        private int lastElapsedMs = 0;

        public frmMain()
        {
            InitializeComponent();
            DoubleBuffered = true;

            time = Stopwatch.StartNew();

            // Create the objects
            objects.Add(new Ball());
            objects.Add(new Racket(5));
            objects.Add(new Racket(200));

            // Add the Paint event
            foreach (PaintableObject po in objects)
                Paint += po.Paint;
        }

        /// <summary>
        /// Invalidate the form
        /// </summary>
        private void fps_Tick(object sender, EventArgs e)
        {
            int ms = Convert.ToInt32(time.ElapsedMilliseconds - lastElapsedMs);
            lastElapsedMs += ms;

            foreach (PaintableObject po in objects)
                po.Move(ms);

            Invalidate();
        }

        private void frmMain_Paint(object sender, PaintEventArgs e)
        {
            // Anti-aliazing
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
        }
    }
}
