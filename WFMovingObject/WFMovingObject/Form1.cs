﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFMovingObject
{
    public partial class Form1 : Form
    {
        List<MovingObject> objects;
        int animationState = 0;

        const int SIZE = 600;

        public Form1()
        {
            InitializeComponent();

            DoubleBuffered = true;

            Random rnd = new Random();

            objects = new List<MovingObject>();

            for (int i = 0; i <= 1000; i++)
            {
                double angle = rnd.Next(0, 360) * Math.PI / 180.0;

                PointF start = new PointF(SIZE / 2, SIZE / 2);
                PointF end = new PointF()
                {
                    X = (float)(start.X + Math.Cos(angle) * SIZE / 2),
                    Y = (float)(start.Y + Math.Sin(angle) * SIZE / 2),
                };

                MovingObject mo = new MovingObject(start, end, rnd.Next(1000, 5000));

                this.Paint += mo.PaintLines;
                Paint += mo.PaintCircles;

                objects.Add(mo);
            }

            //foreach (MovingObject mo in objects)
            //    Paint += mo.PaintCircles;
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            e.Graphics.Clear(Control.DefaultBackColor);

            animationState += 2;

            //foreach (MovingObject mo in objects)
            //{
            //    mo.StartPosition = new PointF(0, (float)Math.Cos(animationState * Math.PI / 180.0) * 300 + 300);
            //}
        }

        private void fps_Tick(object sender, EventArgs e)
        {
            Invalidate();
        }
    }
}
