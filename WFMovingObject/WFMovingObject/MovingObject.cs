﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFMovingObject
{
    /// <summary>
    /// Moving object with a start position, and end position, and time
    /// </summary>
    class MovingObject
    {
        Stopwatch _sw;
        long _timeMs;

        private object _lock = new Object();

        /// <summary>
        /// Start position
        /// </summary>
        public PointF StartPosition { get; set; }

        /// <summary>
        /// End position
        /// </summary>
        public PointF EndPosition { get; set; }

        /// <summary>
        /// Get the current object position
        /// </summary>
        public PointF Position
            => new PointF()
            {
                X = (float)(StartPosition.X + (EndPosition.X - StartPosition.X) * Progress),
                Y = (float)(StartPosition.Y + (EndPosition.Y - StartPosition.Y) * Progress),
            };

        /// <summary>
        /// Get the current move progress (from 0.0 to 1.0)
        /// </summary>
        public double Progress
        {
            get
            {
                double elapsedMs = Convert.ToDouble(_sw.ElapsedMilliseconds);

                if (elapsedMs > _timeMs)
                {
                    _sw.Restart();

                    PointF temp = EndPosition;
                    EndPosition = StartPosition;
                    StartPosition = temp;

                    return 0;
                }

                return elapsedMs / _timeMs;
            }
        }

        #region Constructors

        public MovingObject(PointF from, PointF to, long ms)
        {
            StartPosition = from;
            EndPosition = to;

            _sw = Stopwatch.StartNew();
            _timeMs = ms;
        }

        public MovingObject() : this(0, 0, 100, 100, 5000) { }

        public MovingObject(Point from, Point to, long ms) : this((PointF)from, (PointF)to, ms) { }

        public MovingObject(int xStart, int yStart, int xEnd, int yEnd, long ms) : this(new PointF(xStart, yStart), new PointF(xEnd, yEnd), ms) { }

        #endregion

        /// <summary>
        /// Draw the object line
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">PaintEventArgs object</param>
        public void PaintLines(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawLine(Pens.Orange, StartPosition, EndPosition);
        }

        /// <summary>
        /// Draw the object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">PaintEventArgs object</param>
        public void PaintCircles(object sender, PaintEventArgs e)
        {
            float size = 10;
            e.Graphics.FillEllipse(Brushes.Red, Position.X - size / 2, Position.Y - size / 2, size, size);
        }
    }
}
