﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFRotate
{
    public partial class Form1 : Form
    {
        int angle => tbAngle.Value;
        double angleRad => angle * Math.PI / 180;

        Point centerOfRotation = new Point(20, 20);

        public Form1()
        {
            InitializeComponent();
            DoubleBuffered = true;
        }

        private void tbAngle_ValueChanged(object sender, EventArgs e)
        {
            lblAngle.Text = $"{angle}°";
            Invalidate();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            RectangleF d = new RectangleF(50F, 50F, 200F, 200F);

            List<PointF> points = new List<PointF>() { new PointF(d.Left, d.Top), new PointF(d.Right, d.Top), new PointF(d.Right, d.Bottom), new PointF(d.Left, d.Bottom) };
            points = points.Select(p => RotateAroundCenter(p, centerOfRotation, angleRad)).ToList();

            e.Graphics.FillPolygon(Brushes.DarkSlateGray, points.ToArray());
            e.Graphics.DrawRectangle(Pens.Black, new Rectangle(centerOfRotation, new Size(1, 1)));
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            centerOfRotation = new Point(e.X, e.Y);
            Invalidate();
        }

        private PointF RotateAroundCenter(PointF point, PointF centerOfRotation, double angleRotation)
        {
            double distanceX = point.X - centerOfRotation.X;
            double distanceY = point.Y - centerOfRotation.Y;
            double distance = Math.Sqrt(Math.Pow(distanceX, 2) + Math.Pow(distanceY, 2));

            double angleDistance = Math.Atan2(distanceY, distanceX) + angleRotation;

            return new PointF() {
                X = (float)(centerOfRotation.X + Math.Cos(angleDistance) * distance),
                Y = (float)(centerOfRotation.Y + Math.Sin(angleDistance) * distance),
            };
        }
    }
}
