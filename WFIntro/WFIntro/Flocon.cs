﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace WFIntro
{
    class Flocon
    {
        float _top;
        float _left;
        float _height;
        float _width;
        float _speed;

        int _branchCount = 8;

        int _animationState = 0;

        public Flocon()
        {
            _top = 10;
            _left = 10;
            _height = 10;
            _width = 10;
            _speed = 5;
        }

        public bool IsDown
        {
            get
            {
                return _top > Screen.PrimaryScreen.WorkingArea.Height + _height;
            }
        }

        public Flocon(float height, float width, float left, float speed, int animationState, int branchCount)
        {
            _height = height;
            _width = width;

            this._top = 0 - _height;
            this._left = left;

            this._speed = speed;

            this._animationState = animationState;
            this._branchCount = branchCount;
        }

        public void Paint(object sender, PaintEventArgs e)
        {
            Pen p = new Pen(Color.LightBlue, 2);

            for (double i = 0.0; i < 360.0; i += 360.0 / _branchCount)
                DrawBranch(p, e, i);
        }

        private void DrawBranch(Pen p, PaintEventArgs e, double angle)
        {
            float left = _left + (float)(Math.Sin(_animationState * 10 % 360 * Math.PI / 180) * 10);

            PointF start = new PointF()
            {
                X = left + _width / 2,
                Y = _top + _height / 2
            };

            PointF end = AddToPoint(start, angle, _width / 2);
            PointF middle = AddToPoint(start, angle, _width / 3);

            // DrawLine(p, e, start, angle, _width / 2);
            e.Graphics.DrawLine(p, start, end);
            e.Graphics.DrawLine(p, middle, AddToPoint(middle, angle + (90 / _branchCount * 3), _width / 8));
            e.Graphics.DrawLine(p, middle, AddToPoint(middle, angle - (90 / _branchCount * 3), _width / 8));
        }

        private PointF AddToPoint(PointF point, double angle, float length)
        {
            return new PointF()
            {
                X = point.X + (float)(Math.Cos(DegToRad(angle)) * length),
                Y = point.Y + (float)(Math.Sin(DegToRad(angle)) * length),
            };
        }

        private double DegToRad(double deg)
        {
            return deg * Math.PI / 180.0;
        }

        public void Tombe()
        {
            _animationState += 1;

            _top += _speed;
            _speed += (float)0.3;

            // Debug one snowflake
            //_top = 10;
            //_left = 10;
            //_width = 500;
            //_height = 500;
        }
    }
}
