﻿namespace WFIntro
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.fps = new System.Windows.Forms.Timer(this.components);
            this.VitesseChute = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // fps
            // 
            this.fps.Enabled = true;
            this.fps.Interval = 16;
            this.fps.Tick += new System.EventHandler(this.fps_Tick);
            // 
            // VitesseChute
            // 
            this.VitesseChute.Enabled = true;
            this.VitesseChute.Interval = 20;
            this.VitesseChute.Tick += new System.EventHandler(this.VitesseChute_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(38)))), ((int)(((byte)(74)))));
            this.ClientSize = new System.Drawing.Size(828, 555);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "Form1";
            this.TransparencyKey = System.Drawing.Color.Red;
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer fps;
        private System.Windows.Forms.Timer VitesseChute;
    }
}

