﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFIntro
{
    public partial class Form1 : Form
    {
        List<Flocon> neige = new List<Flocon>();
        Random rnd = new Random();

        const int MAX_SNOWFLAKES_COUNT = 1000000;

        public Form1()
        {
            InitializeComponent();
            DoubleBuffered = true;
            WindowState = FormWindowState.Maximized;
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(Color.Red);

        }

        private void fps_Tick(object sender, EventArgs e)
        {
            List<Flocon> aEnlever = new List<Flocon>();

            foreach (Flocon ft in neige)
                if (ft.IsDown)
                    aEnlever.Add(ft);

            foreach (Flocon ft in aEnlever)
                this.Paint -= ft.Paint;

            neige.RemoveAll(f => f.IsDown);

            Invalidate();
        }

        private void VitesseChute_Tick(object sender, EventArgs e)
        {
            if (neige.Count < MAX_SNOWFLAKES_COUNT)
            {
                for (int i = 1; i >= 1; i--)
                {
                    int size = rnd.Next(10, 50);
                    Flocon f = new Flocon(size, size, rnd.Next(0, Width), (float)rnd.NextDouble(), rnd.Next(360), rnd.Next(6, 16));
                    neige.Add(f);
                    this.Paint += f.Paint;
                }

            }


            foreach (Flocon ft in neige)
                ft.Tombe();
        }
    }
}
