﻿namespace WFMovingObject
{
    partial class frmMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.fps = new System.Windows.Forms.Timer(this.components);
            this.keyboard = new System.Windows.Forms.Timer(this.components);
            this.btnReverse = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // fps
            // 
            this.fps.Enabled = true;
            this.fps.Interval = 16;
            this.fps.Tick += new System.EventHandler(this.fps_Tick);
            // 
            // keyboard
            // 
            this.keyboard.Enabled = true;
            this.keyboard.Interval = 10;
            // 
            // btnReverse
            // 
            this.btnReverse.Location = new System.Drawing.Point(547, 13);
            this.btnReverse.Name = "btnReverse";
            this.btnReverse.Size = new System.Drawing.Size(75, 23);
            this.btnReverse.TabIndex = 0;
            this.btnReverse.Text = "Reverse";
            this.btnReverse.UseVisualStyleBackColor = true;
            this.btnReverse.Click += new System.EventHandler(this.btnReverse_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 611);
            this.Controls.Add(this.btnReverse);
            this.Name = "frmMain";
            this.Text = "Form1";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer fps;
        private System.Windows.Forms.Timer keyboard;
        private System.Windows.Forms.Button btnReverse;
    }
}

