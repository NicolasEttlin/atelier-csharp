﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace WFMovingObject
{
    class MovingSquare : MovingObject
    {
        public MovingSquare(int xStart, int yStart, int xEnd, int yEnd, long ms) : base(new PointF(xStart, yStart), new PointF(xEnd, yEnd), ms) { }


        /// <summary>
        /// Draw the object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">PaintEventArgs object</param>
        public override void Paint(object sender, PaintEventArgs e)
        {
            base.Paint(sender, e);

            float size = 15;
            e.Graphics.FillRectangle(Brushes.Green, Position.X - size / 2, Position.Y - size / 2, size, size);
        }
    }
}
