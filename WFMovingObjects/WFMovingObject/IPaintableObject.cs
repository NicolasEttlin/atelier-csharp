﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFMovingObject
{
    interface IPaintableObject
    {
        /// <summary>
        /// Paint the object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Paint(object sender, PaintEventArgs e);

        /// <summary>
        /// Should the object be removed ?
        /// </summary>
        bool IsObsolete { get; }
    }
}
