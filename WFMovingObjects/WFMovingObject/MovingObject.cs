﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFMovingObject
{
    /// <summary>
    /// Moving object with a start position, and end position, and time
    /// </summary>
    class MovingObject : IPaintableObject
    {
        protected ReversibleStopwatch _sw;
        protected long _timeMs; // Current move length

        /// <summary>
        /// Start position
        /// </summary>
        public PointF StartPosition { get; set; }

        /// <summary>
        /// End position
        /// </summary>
        public PointF EndPosition { get; set; }

        /// <summary>
        /// Get the current object position
        /// </summary>
        public PointF Position
        {
            get
            {
                double progress = Progress;
                PointF startPosition = StartPosition;
                PointF endPosition = EndPosition;

                return new PointF()
                {
                    X = (float)(progress * (endPosition.X - startPosition.X) + startPosition.X),
                    Y = (float)(progress * (endPosition.Y - startPosition.Y) + startPosition.Y),
                };
            }
        }

        /// <summary>
        /// Get the current move progress (from 0.0 to 1.0)
        /// </summary>
        public double Progress
        {
            get
            {
                double _elapsedMs = Convert.ToDouble(_sw.CounterMs);

                if (_elapsedMs < 0 || _elapsedMs >= _timeMs)
                {
                    return ExceedTime(_elapsedMs);
                }

                return _elapsedMs / _timeMs;
            }
        }

        /// <summary>
        /// Handle time exceed
        /// </summary>
        /// <param name="elapsedMs">Elapsed milliseconds</param>
        /// <returns>Progress</returns>
        protected virtual double ExceedTime(double elapsedMs)
        {
            // Base class does nothing and lets the object continue.
            return elapsedMs / _timeMs;
        }

        /// <summary>
        /// Should the object be removed ?
        /// </summary>
        public virtual bool IsObsolete { get => false; }

        #region Constructors

        public MovingObject(PointF from, PointF to, long ms)
        {
            StartPosition = from;
            EndPosition = to;

            _sw = ReversibleStopwatch.StartNew();
            _timeMs = ms;
        }

        public MovingObject() : this(0, 0, 100, 100, 5000) { }

        public MovingObject(Point from, Point to, long ms) : this((PointF)from, (PointF)to, ms) { }

        public MovingObject(int xStart, int yStart, int xEnd, int yEnd, long ms) : this(new PointF(xStart, yStart), new PointF(xEnd, yEnd), ms) { }

        #endregion

        /// <summary>
        /// Draw lines showing the current path
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">PaintEventArgs object</param>
        public virtual void Paint(object sender, PaintEventArgs e)
        {
            PointF position = Position;
            e.Graphics.DrawLine(Pens.Orange, position, EndPosition); // Orange : remaining
            e.Graphics.DrawLine(Pens.YellowGreen, position, StartPosition); // Green : done
        }
    }
}
