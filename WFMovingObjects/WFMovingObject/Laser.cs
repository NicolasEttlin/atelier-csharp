﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace WFMovingObject
{
    class Laser : MovingObject
    {
        const int LENGTH = 30;

        public Laser(int startX, int startY, int ms) : base(startX, startY, startX, -LENGTH, ms) { }

        public override bool IsObsolete
        {
            get
            {
                return Position.Y <= 0 - LENGTH;
            }
        }

        /// <summary>
        /// Draw the object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">PaintEventArgs object</param>
        public override void Paint(object sender, PaintEventArgs e)
        {
            PointF position = Position;
            e.Graphics.DrawLine(Pens.Red, position, new PointF() {
                X = position.X,
                Y = Math.Min(position.Y + LENGTH, StartPosition.Y),
            });
        }
    }
}
