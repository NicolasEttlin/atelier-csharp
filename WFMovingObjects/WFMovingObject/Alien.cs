﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace WFMovingObject
{
    class Alien : MovingObject
    {
        const int SIZE = 50;

        private List<PointF> _path;
        private int _currentPathIndex = 0;
        private int _totalTimeMs;

        public Alien(List<PointF> path, int totalTimeMs) : base()
        {
            _path = path;
            _totalTimeMs = totalTimeMs;
            UpdatePath();
        }

        /// <summary>
        /// Move to the next path segment
        /// </summary>
        /// <returns></returns>
        protected override double ExceedTime(double _)
        {
            _currentPathIndex = (_currentPathIndex + 1) % _path.Count;
            UpdatePath();
            return 0.0;
        }

        /// <summary>
        /// Draw the object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">PaintEventArgs object</param>
        public override void Paint(object sender, PaintEventArgs e)
        {
            DrawPath(e.Graphics);

            PointF position = Position;
            e.Graphics.DrawPolygon(Pens.Black, new PointF[] {
                new PointF() { X = position.X, Y = position.Y - SIZE / 2 }, // Top
                new PointF() { X = position.X - SIZE / 2, Y = position.Y + SIZE / 2 }, // Bottom left
                new PointF() { X = position.X + SIZE / 2, Y = position.Y + SIZE / 2 }, // Bottom right
            });
        }

        /// <summary>
        /// Draw all the paths
        /// </summary>
        /// <param name="g">e.Graphics</param>
        private void DrawPath(Graphics g)
        {
            for (int i = 0; i < _path.Count; i++)
            {
                g.DrawLine(
                    i == _currentPathIndex ? Pens.Green : Pens.Black,
                    _path[i],
                    _path[i + 1 == _path.Count ? 0 : i + 1]
                );
            }
        }

        /// <summary>
        /// Reset start and end position
        /// </summary>
        private void UpdatePath()
        {
            // Set the current path to the next path segment
            StartPosition = _path[_currentPathIndex];
            EndPosition = _path[_currentPathIndex + 1 == _path.Count ? 0 : _currentPathIndex + 1];

            // Set the current move time proportionally to
            _timeMs = (int)(Geometry.Distance(StartPosition, EndPosition) / Geometry.TotalPathLength(_path) * _totalTimeMs);

            _sw.Restart();
        }
    }
}
