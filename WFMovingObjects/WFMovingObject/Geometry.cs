﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace WFMovingObject
{
    /// <summary>
    /// Geometric computations
    /// </summary>
    public static class Geometry
    {
        /// <summary>
        /// Get the distance (in pixels) between two points
        /// </summary>
        public static float Distance(PointF p1, PointF p2)
        {
            float a = p1.X - p2.X;
            float b = p1.Y - p2.Y;

            return (float)Math.Sqrt(a * a + b * b);
        }

        /// <summary>
        /// Get the total path distance
        /// </summary>
        public static float TotalPathLength(List<PointF> path)
        {
            float result = 0;

            for (int i = 0; i < path.Count; i++)
            {
                result += Distance(path[i], path[i + 1 == path.Count ? 0 : i + 1]);
            }

            return result;
        }
    }
}
