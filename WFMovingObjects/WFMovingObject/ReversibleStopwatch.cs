﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace WFMovingObject
{
    class ReversibleStopwatch : Stopwatch
    {
        private long _lastSavedElapsedMs = 0;
        private long _counter = 0;

        public bool GoingForward { get; set; } = true;

        public void Reverse()
        {
            GoingForward = !GoingForward;
        }

        public long CounterMs
        {
            get
            {
                if (GoingForward)
                    _counter += DeltaMs;
                else
                    _counter -= DeltaMs;

                return _counter;
            }
        }

        public new static ReversibleStopwatch StartNew()
        {
            ReversibleStopwatch rsw = new ReversibleStopwatch();
            rsw.Start();
            return rsw;
        }

        /// <summary>
        /// Get the time elapsed since the last time we got this property
        /// </summary>
        private long DeltaMs
        {
            get
            {
                long delta = ElapsedMilliseconds - _lastSavedElapsedMs;
                _lastSavedElapsedMs += delta;
                return delta;
            }
        }
    }
}
