﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFMovingObject
{
    public partial class frmMain : Form
    {
        List<IPaintableObject> objects;
        Random rnd = new Random();
        Vessel _vessel = new Vessel();

        public frmMain()
        {
            InitializeComponent();
            DoubleBuffered = true;

            objects = new List<IPaintableObject>();

            // Vessel
            objects.Add(_vessel);

            // Alien
            List<PointF> path = new List<PointF>();
            for (int i = 0; i < rnd.Next(4, 8); i++)
            {
                path.Add(new PointF(rnd.Next(500), rnd.Next(500)));
            }

            objects.Add(new Alien(path, 5000));

            // Paint
            foreach (IPaintableObject mo in objects)
                Paint += mo.Paint;
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(DefaultBackColor);
        }

        /// <summary>
        /// Redraw the image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fps_Tick(object sender, EventArgs e)
        {
            // Shoot a new laser
            PointF pos = _vessel.Position;
            Laser l = new Laser((int)pos.X, (int)pos.Y - Vessel.SIZE / 2, 1000);
            objects.Add(l);
            Paint += l.Paint;

            // Remove obsolete objects
            List<IPaintableObject> obsoleteObjects = objects.Where(o => o.IsObsolete).ToList();

            foreach (IPaintableObject o in obsoleteObjects)
                Paint -= o.Paint;

            objects.RemoveAll(o => o.IsObsolete);

            // Redraw the form
            Invalidate();
        }

        private void btnReverse_Click(object sender, EventArgs e)
        {
            _vessel.Reverse();
        }
    }
}
