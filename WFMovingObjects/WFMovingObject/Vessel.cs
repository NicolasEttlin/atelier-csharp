﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace WFMovingObject
{
    /// <summary>
    /// Hero
    /// </summary>
    class Vessel : MovingObject
    {
        public const int SIZE = 15;
        const int Y_POSITION = 600;
        const int SPEED = 10;

        public override bool IsObsolete { get => false; }

        public Vessel() : base (new PointF(100, Y_POSITION), new PointF(500, Y_POSITION), 1000)
        {
        }

        Brush color = Brushes.Green;

        /// <summary>
        /// Draw the object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">PaintEventArgs object</param>
        public override void Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.FillRectangle(color, Position.X - SIZE / 2, Position.Y - SIZE / 2, SIZE, SIZE);
        }

        /// <summary>
        /// Handle time exceed
        /// </summary>
        /// <param name="elapsedMs">Elapsed milliseconds</param>
        /// <returns>Progress</returns>
        protected override double ExceedTime(double elapsedMs)
        {
            _sw.Reverse();

            if (elapsedMs < 0)
                elapsedMs = 0;
                // elapsedMs = Math.Abs(elapsedMs);

            if (elapsedMs > _timeMs)
                elapsedMs = _timeMs;
                // elapsedMs -= 2 * (elapsedMs - _timeMs);

            return elapsedMs / _timeMs;
        }

        public void Reverse()
        {
            _sw.Reverse();
        }
    }
}
